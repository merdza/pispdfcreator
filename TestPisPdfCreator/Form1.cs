﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PisPdfCreator;

namespace TestPisPdfCreator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Invoice invoice;
            string pdfPath = @"C:\Git\PisPdfCreator\TestPisPdfCreator\CreatedFiles\" + Utils.GetSerbianTimeStampString(DateTime.Now, false, StringSeparator.hyphen) + @".pdf";

            //Opsti podaci o dokumentu
            //iz nekog razloga ne prihvata nasa slova, ni cirilicu - samo celava latinica
            DocumentMetaData documentMetaData = new DocumentMetaData(
                @"Racun za ishranu, smestaj i vaspitni rad",
                @"Gaudeamus software",
                @"Racun Ministarstvu prosvete za ishranu, smestaj i vaspitni rad ucenika i studenata",
                @"Republika Srbija, Ministarstvo prosvete, Domovi ucenika, Racun, Faktura, Digitalni potpis"
                );

            //podaci o posiljaocu
            InvoiceParty invoiceSender = new InvoiceParty(
                @"Школа са домом за ученике оштећеног слуха и говора ""11. мај"" Јагодина Школа са домом за ученике оштећеног слуха и говора ""11. мај"" Јагодина",
                "",//nameSup
                @"",
                @"Улица Београдска бб",
                @"24000",
                @"Јагодина",
                @"Телефон: 024/123-456",
                @"",
                @"",
                @"12345678",
                @"12345678",
                @"840-123456-12"
                );

            //podaci o primaocu, u ovom slucaju ostaju takvi kakvi su navedeni ovde
            InvoiceParty invoiceReceiver = new InvoiceParty(
                @"Министарство просвете, науке и технолошког развоја Републике Србије",
                "",//nameSup
                @"Сектор за ученички и студентски стандард",
                @"Захумска 14",
                @"11000",
                @"Београд",
                @"",
                @"",
                @"",
                @"17329235",
                @"102199748",
                @""
                );

            //Opsti podaci o racunu
            InvoiceMetaData invoiceTitleData = new InvoiceMetaDataDomoviUcenika(
                @"1234-2017",
                @"30.02.2017.",
                @"Јагодина",
                @"фебруар",
                @"2017.",
                @"у складу са законом",
                @"Lorem ipsum dolor sit amet, ea sed prompta mentitum. An cum errem scripserit. Te vim atqui molestie, eu eum ceteros maiorum. Case tempor vivendum ei eum. Mel dicam apeirian electram ex. Pri expetenda dignissim suscipiantur ne, populo scripserit interpretaris ius ea, qui viderer delenit ut. Novum adipisci eu eum, minimum invenire qui in, quodsi eligendi posidonium mea no. Ea cum eruditi qualisque. Nam ne aperiri insolens instructior, ius viris epicurei ne. Ex viris virtute noluisse mei, sea ut illud graeci patrioque, voluptaria rationibus reprimique quo ut."
                );


            //podaci u tabeli racuna
            InvoiceData invoiceData = new InvoiceDataDomoviUcenika(
                @"3.717",
                @"51,00",
                @"189.567,00",
                @"3.727",
                @"116,00",
                @"432.332,00",
                @"3.927",
                @"97,00",
                @"380.919,00",
                @"1.002.818,00",
                @"335",
                @"123",
                @"8.827,00",
                @"2.957.045,00",
                @"400,00",
                @"134.000,00",
                @"7.184.908,00"
                );

            //podaci o potpisniku
            Signer signerData = new Signer(
                @"Петар Петровић",
                @"директор",
                "potpisnik"
                );


            List<InvoiceAnexDataRow> invoiceAnexDataRows = new List<InvoiceAnexDataRow>();
            invoiceAnexDataRows.Add(new InvoiceAnexDataRow()
            {
                Column0 = "Р. бр.",
                Column1 = "ДАТУМ",
                Column2 = "ДОРУЧАК",
                Column3 = "РУЧАК",
                Column4 = "ВЕЧЕРА",
                Column5 = "УКУПНО"
            });

            for (int i = 1; i <= 31; i++)
            {
                invoiceAnexDataRows.Add(new InvoiceAnexDataRow()
                {
                    Column0 = i + ".",
                    Column1 = i + ".12.2020.",
                    Column2 = (32 + 2*i).ToString(),
                    Column3 = (75 + 3*i).ToString(),
                    Column4 = (46 + 2*i).ToString(),
                    Column5 = (264 + 4*i).ToString()
                });
            }
            

            invoiceAnexDataRows.Add(new InvoiceAnexDataRow()
            {
                Column0 = "",
                Column1 = "УКУПНО ЗА ПЕРИОД:",
                Column2 = "1332",
                Column3 = "1581",
                Column4 = "1407",
                Column5 = "4320"
            });

            InvoiceAnexData invoiceAnexData = new InvoiceAnexData()
            {
                InvoicePeriod = "За период од 1.10.2020. до 31.10.2020.",
                GraphicSignatureImagePath = @"../../../TestPisPdfCreator/SlikePotpisa/Ruski Krstur potpis i pecat.jpg",
                InvoiceAnexDataRows = invoiceAnexDataRows
            };

            string IDUserType = "2";

            if (IDUserType.Equals("1"))
            {
                invoice = new InvoiceDomoviUcenika(documentMetaData, invoiceSender, invoiceReceiver, invoiceTitleData, invoiceData, signerData, invoiceAnexData);
            }
            else
            {
                invoice = new InvoiceDomoviUcenikaStudenti(documentMetaData, invoiceSender, invoiceReceiver, invoiceTitleData, invoiceData, signerData, invoiceAnexData);
            }

            PdfCreator.CreatePdf(invoice, pdfPath);
        }

    }
}
