﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace TestPisPdfCreator
    {
        public static class Utils
        {
            public static void GetCultureNames()
            {
                // ispisi u konzolu kodove svih kulture
                List<string> list = new List<string>();
                foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
                {
                    string specName = "(none)";
                    try { specName = CultureInfo.CreateSpecificCulture(ci.Name).Name; }
                    catch { }
                    list.Add(String.Format("{0,-12}{1,-12}{2}", ci.Name, specName, ci.EnglishName));
                }

                list.Sort();  // sort by name

                // write to console
                Console.WriteLine("CULTURE   SPEC.CULTURE  ENGLISH NAME");
                Console.WriteLine("--------------------------------------------------------------");
                foreach (string str in list)
                    Console.WriteLine(str);
            }

            public static string AddLeadingZeros(int i, int totalNumbers)
            {
                try
                {
                    int mul = 10;
                    string lead = "";
                    for (int j = totalNumbers; j > 1; j--)
                    {
                        if (i < mul)
                        {
                            for (int k = 0; k < j - 1; k++)
                            {
                                lead += "0";
                            }
                            return lead + i.ToString();
                        }
                        mul *= 10;
                    }
                    return i.ToString();
                }
                catch (Exception)
                {
                    //ma ovo nema sanse da se desi
                    //try catch je samo da ne pukne program
                    return "0";
                }
            }

            public static string MakeFixedLengthString(string s, int length)
            {
                //koristi se za pravljenje stringa fiksne duzine koji se npr. koristi za racune
                //ako je string kraci od potrebne duzine, dodaju mu se spejsovi
                //ako je duzi, skracuje se na potrebnu duzinu
                try
                {
                    if (s.Length < length)
                    {
                        int len = s.Length;
                        for (int i = 0; i < length - len; i++)
                        {
                            s += " ";
                        }
                    }
                    else if (s.Length > length)
                    {
                        s = s.Substring(0, length);
                    }
                    return s;
                }
                catch (Exception)
                {
                    //ma ovo nema sanse da se desi
                    //try catch je samo da ne pukne program
                    return "0";
                }
            }

            /// <summary>
            /// Get a string which is located between two strings in a larger string.
            /// </summary>
            /// <param name="inputString">String to parse</param>
            /// <param name="firstString">String which should be the beginning border</param>
            /// <param name="secondString">String which should be the ending border</param>
            /// <param name="endingRequired">Whether second string has to appear at the end of a input string. If false, after finding firstString, and second is not present, return string will be till the end</param>
            /// <returns>Resulting string beween firstString and Second</returns>
            public static string GetStringBetweenTwoStrings(string inputString, string firstString, string secondString, bool endingRequired)
            {
                string result = string.Empty;

                int firstStringPosition = inputString.IndexOf(firstString);
                if (firstStringPosition < 0)
                {
                    //nema prvog stringa, vrati prazno
                    return result;
                }

                int positionFrom = firstStringPosition + firstString.Length;
                int positionTo = inputString.IndexOf(secondString, positionFrom);

                if (positionTo < 0)
                {
                    //nema drugog stringa od pocetne pozicije, pa do kraja.
                    if (endingRequired)
                    {
                        // Vrati prazan string
                        return result;
                    }
                    else
                    {
                        // Vrati substring od pocetne pozicije do kraja
                        result = inputString.Substring(positionFrom);
                    }
                }
                else
                {
                    result = inputString.Substring(positionFrom, positionTo - positionFrom);
                }

                return result;
            }

            //ex 15052018
            public static string GetShortSerbianDateNoDots(DateTime dt)
            {
                return Utils.AddLeadingZeros(dt.Day, 2) + Utils.AddLeadingZeros(dt.Month, 2) + Utils.AddLeadingZeros(dt.Year, 2);
            }

            //ex 15.05.2018. ili bez tacke na kraju
            public static string GetShortSerbianDate(DateTime dt, bool dotAfterYear)
            {
                string ret = Utils.AddLeadingZeros(dt.Day, 2) + @"." + Utils.AddLeadingZeros(dt.Month, 2) + @"." + Utils.AddLeadingZeros(dt.Year, 2);
                if (dotAfterYear)
                {
                    ret += @".";
                }
                return ret;
            }

            public static string GetSeparatorString(StringSeparator separator)
            {
                string ret = string.Empty;
                switch (separator)
                {
                    case StringSeparator.dot:
                        ret = @".";
                        break;
                    case StringSeparator.comma:
                        ret = @",";
                        break;
                    case StringSeparator.hyphen:
                        ret = @"-";
                        break;
                    case StringSeparator.underscore:
                        ret = @"_";
                        break;
                    case StringSeparator.slash:
                        ret = @"/";
                        break;
                    case StringSeparator.backslash:
                        ret = @"\";
                        break;
                    case StringSeparator.colon:
                        ret = @":";
                        break;
                    case StringSeparator.semicolon:
                        ret = @";";
                        break;
                    default:
                        break;
                }
                return ret;
            }

            //ex 2018-04-17-12-19-23-563
            public static string GetSerbianTimeStampString(DateTime dt, bool showMilliseconds, StringSeparator separator)
            {
                string sep = GetSeparatorString(separator);
                string ret = AddLeadingZeros(dt.Year, 4) + sep + AddLeadingZeros(dt.Month, 2) + sep + AddLeadingZeros(dt.Day, 2) + sep + AddLeadingZeros(dt.Hour, 2) + sep + AddLeadingZeros(dt.Minute, 2) + sep + AddLeadingZeros(dt.Second, 2);
                if (showMilliseconds)
                {
                    ret += sep + AddLeadingZeros(dt.Millisecond, 3);
                }
                return ret;
            }

            public static string CreateTempFile()
            {
                string fileName = string.Empty;

                try
                {
                    // Get the full name of the newly created Temporary file. 
                    fileName = Path.GetTempFileName();

                    // Craete a FileInfo object to set the file's attributes
                    FileInfo fileInfo = new FileInfo(fileName)
                    {
                        // Set the Attribute property of this file to Temporary. 
                        // Although this is not completely necessary, the .NET Framework is able 
                        // to optimize the use of Temporary files by keeping them cached in memory.
                        Attributes = FileAttributes.Temporary
                    };

                }
                catch (Exception ex)
                {
                    throw new Exception("Unable to create TEMP file or set its attributes: " + ex.Message);
                }

                return fileName;
            }

            public static void DeleteFile(string file)
            {
                try
                {
                    if (file != string.Empty)
                    {
                        if (File.Exists(file))
                        {
                            System.GC.Collect();
                            System.GC.WaitForPendingFinalizers();
                            File.Delete(file);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Deleting file " + file + " failed. " + ex.Message);
                }
            }

            public static void KillProcess(string processName)
            {
                try
                {
                    Process[] processes = Process.GetProcessesByName(processName);
                    foreach (var process in processes)
                    {
                        process.Kill();
                    }
                }
                catch (Exception)
                {
                    //nije uspelo ubijanje procesa. ne treba prijavljivati gresku
                }
            }

            /// <summary>
            /// Converts string to byte array. Each two characters in string represent hex value and becomes one byte with decimal value in resulting byte array
            /// </summary>
            public static byte[] StringToByteArray(String hex)
            {
                int NumberChars = hex.Length;
                byte[] bytes = new byte[NumberChars / 2];
                for (int i = 0; i < NumberChars; i += 2)
                    bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
                return bytes;
            }

            /// <summary>
            /// Converts byte array to string. Each byte from array represents decimal value and becomes hex string of two characters and part of resulting string
            /// </summary>
            public static string ByteArrayToString(byte[] byteArray)
            {
                string ret = string.Empty;

                foreach (var bt in byteArray)
                {
                    ret += bt.ToString("X2");
                }
                return ret;
            }

            /// <summary>
            /// converts hex string to decimal
            /// </summary>
            public static string FromHex(string hex)
            {
                byte[] responseString = StringToByteArray(hex);
                return System.Text.Encoding.UTF8.GetString(responseString);
            }

            /// <summary>
            /// returns current time in miliseconds
            /// </summary>
            /// <returns></returns>
            public static long CurrentTimeMillis()
            {
                DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                return (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
            }

            /// <value>Size of the buffer for converting stream to byte array</value>
            public static readonly int BufferSize = 4096 * 8;

            /// <summary>
            /// Sends HttpWebRequest via post method and returns response in a form of byte array
            /// </summary>
            public static byte[] PostData(string url, byte[] data, string contentType, string accept)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = contentType;
                request.ContentLength = data.Length;
                request.Accept = accept;
                Stream stream = request.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream respStream = response.GetResponseStream();
                byte[] resp = ToByteArray(respStream);
                respStream.Close();

                return resp;
            }

            /// <summary>
            /// Converts stream to byte array
            /// </summary>
            public static byte[] ToByteArray(Stream stream)
            {
                byte[] buffer = new byte[BufferSize];
                MemoryStream ms = new MemoryStream();

                int read = 0;

                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }


         

            public static object GetPropertyValue(object src, string propName)
            {
                return src.GetType().GetProperty(propName).GetValue(src, null);
            }

            /// <summary>
            /// Checks if input string is valid path or not
            /// </summary>
            /// <param name="inputString"></param>
            /// <returns></returns>
            public static bool CheckIfCorrectPath(string inputString)
            {
                try
                {
                    Path.GetFullPath(inputString);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public enum StringSeparator
        {
            none,
            dot,
            comma,
            hyphen,
            underscore,
            slash,
            backslash,
            colon,
            semicolon
        }


    }
