﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisPdfCreator
{
    public class InvoiceDataMinistarstvoZbirni : InvoiceData
    {
        //List<InvoiceDataMinistarstvoZbirniPodaciDoma> Do
        public List<InvoiceDataMinistarstvoZbirniPodaciDoma> ListaDomovaZaFakturu { get; set; }

        public InvoiceDataMinistarstvoZbirni(List<InvoiceDataMinistarstvoZbirniPodaciDoma> list, string totalPrice):base(totalPrice)
        {
            ListaDomovaZaFakturu = list;
        }
    }
}
