﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisPdfCreator
{
    public class InvoiceData
    {
        public string Total { get; set; }

        public InvoiceData(string total)
        {
            Total = total;
        }
    }
}
