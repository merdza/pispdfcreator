﻿using System;
using System.Collections.Generic;
using System.Text;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;
using System.Xml.XPath;

namespace PisPdfCreator
{
    public class InvoiceMinistarstvoZbirniUcenici : Invoice
    {
        protected Table tableSender;
        public new InvoiceMetaDataDomoviUcenika InvoiceTitleData { get; set; }
        public new InvoiceDataMinistarstvoZbirni InvoiceContent { get; set; }

        public Signer Signer1 { get; set; }
        public Signer Signer2 { get; set; }
        public Signer Signer4 { get; set; }

        public InvoiceMinistarstvoZbirniUcenici(DocumentMetaData documentMetaData, InvoiceParty sender, InvoiceParty receiver, InvoiceMetaData invoiceMeta, InvoiceData invoiceData, Signer signer1, Signer signer2, Signer signer3, Signer signer4)
            : base(documentMetaData, sender, null, invoiceMeta, invoiceData, signer3)
        {
            InvoiceTitleData = (InvoiceMetaDataDomoviUcenika)invoiceMeta;
            InvoiceContent = (InvoiceDataMinistarstvoZbirni)invoiceData;
            Signer1 = signer1;
            Signer2 = signer2;
            Signer4 = signer4;        
        }

        public override Document CreateDocument()
        {
            this.document = new Document();
            // Each MigraDoc document needs at least one section.
            Section section = this.document.AddSection();
            section.PageSetup.RightMargin = "1cm";
            section.PageSetup.LeftMargin = "1cm";
            section.PageSetup.TopMargin = "1cm";
            section.PageSetup.BottomMargin = "3cm";

            //////////////////////////////////////////////
            //   POŠILJALAC
            //////////////////////////////////////////////
            //this.senderFrame = section.AddTextFrame();
            //this.senderFrame.Height = "4cm";
            //this.senderFrame.Width = "7.0cm";
            //this.senderFrame.Left = ShapePosition.Left;
            //this.senderFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            //this.senderFrame.Top = "2.0cm";
            //this.senderFrame.RelativeVertical = RelativeVertical.Page;
            ////this.invoiceTitleFrame.LineFormat.Width = "0.1mm";

            //this.tableSender = senderFrame.AddTable();

            tableSender = section.AddTable();

            tableSender.Style = "Table";
            //this.table.Borders.Color = TableBorder;
            //this.table.Borders.Width = 0.25;
            //this.table.Borders.Left.Width = 0.5;
            //this.table.Borders.Right.Width = 0.5;
            tableSender.Rows.LeftIndent = 0;

            // Prvo moraju da se naprave kolone, odnosno ovde jedna kolona
            Column column = tableSender.AddColumn("7cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Prvi red u tabeli
            Row row = tableSender.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            //row.Shading.Color = TableBlue;

            Paragraph paragraphSender = row.Cells[0].AddParagraph();
            Image image = paragraphSender.AddImage(Sender.Logo);  // row.Cells[0].AddImage(Sender.Logo);  ovako ne moze da se centrira
            image.Height = "2.5cm";
            image.LockAspectRatio = true;

            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            


            //novi red
            row = tableSender.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            paragraphSender = row.Cells[0].AddParagraph();
            paragraphSender.Format.Font.Name = "Times New Roman";
            paragraphSender.Format.Font.Size = 10;
            paragraphSender.AddText(Sender.NameSup);
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            //novi red
            row = tableSender.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            //row.Shading.Color = TableBlue;
            paragraphSender = row.Cells[0].AddParagraph();
            paragraphSender.Format.Font.Name = "Times New Roman";
            paragraphSender.Format.Font.Size = 10;
            paragraphSender.AddText(Sender.Name.ToUpper());
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            //novi red
            row = tableSender.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            //row.Shading.Color = TableBlue;
            paragraphSender = row.Cells[0].AddParagraph();
            paragraphSender.Format.Font.Name = "Times New Roman";
            paragraphSender.Format.Font.Size = 10;
            paragraphSender.AddText(Sender.NameSub);
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            //novi red
            row = tableSender.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            //row.Shading.Color = TableBlue;
            paragraphSender = row.Cells[0].AddParagraph();
            paragraphSender.Format.Font.Name = "Times New Roman";
            paragraphSender.Format.Font.Size = 10;
            paragraphSender.AddText("Одељење за послове ученичког и студентског стандарда");  //dodato po posebnom zahtevu, pa nije u bazi nego je fiksan tekst ovde. tako je lakse :-)
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            //novi red
            row = tableSender.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            //row.Shading.Color = TableBlue;
            paragraphSender = row.Cells[0].AddParagraph();
            paragraphSender.Format.Font.Name = "Times New Roman";
            paragraphSender.Format.Font.Size = 10;
            paragraphSender.AddText(@"Број: " + InvoiceTitleData.InvoiceNumber);
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            //novi red
            row = tableSender.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            //row.Shading.Color = TableBlue;
            paragraphSender = row.Cells[0].AddParagraph();
            paragraphSender.Format.Font.Name = "Times New Roman";
            paragraphSender.Format.Font.Size = 10;
            paragraphSender.AddText(@"Датум: " + InvoiceTitleData.InvoiceDate);
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            //novi red
            row = tableSender.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            //row.Shading.Color = TableBlue;
            paragraphSender = row.Cells[0].AddParagraph();
            paragraphSender.Format.Font.Name = "Times New Roman";
            paragraphSender.Format.Font.Size = 10;
            paragraphSender.AddText(InvoiceTitleData.InvoicePlaceOfIssuance);
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;


            section.AddParagraph();

            //////////////////////////////////////////////
            //   NASLOV
            //////////////////////////////////////////////
            //this.invoiceTitleFrame = section.AddTextFrame();
            //this.invoiceTitleFrame.Height = "1cm";
            //this.invoiceTitleFrame.Width = "16.0cm";
            //this.invoiceTitleFrame.Left = ShapePosition.Left;
            //this.invoiceTitleFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            //this.invoiceTitleFrame.Top = "8.0cm";
            //this.invoiceTitleFrame.RelativeVertical = RelativeVertical.Page;
            ////this.invoiceTitleFrame.LineFormat.Width = "0.1mm";

            tableInvoiceTitle = section.AddTable();

            this.tableInvoiceTitle.Style = "Table";
            //this.table.Borders.Color = TableBorder;
            //this.table.Borders.Width = 0.25;
            //this.table.Borders.Left.Width = 0.5;
            //this.table.Borders.Right.Width = 0.5;
            this.tableInvoiceTitle.Rows.LeftIndent = 0;

            // Prvo moraju da se naprave kolone, odnosno ovde jedna kolona
            column = this.tableInvoiceTitle.AddColumn("19cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Prvi red u tabeli
            row = tableInvoiceTitle.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            Paragraph paragraphInvoiceTitle = row.Cells[0].AddParagraph();
            paragraphInvoiceTitle.Format.Font.Name = "Times New Roman";
            paragraphInvoiceTitle.Format.Font.Size = 10;
            paragraphInvoiceTitle.AddText(@"ИНТЕРНИ НАЛОГ ЗА ПЛАЋАЊЕ РАЧУНА/ОБАВЕЗА");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;


            //////////////////////////////////////////////
            //   OPIS
            //////////////////////////////////////////////
            //TextFrame descriptionFrame;
            //descriptionFrame = section.AddTextFrame();
            //descriptionFrame.Height = "1cm";
            //descriptionFrame.Width = "16.0cm";
            //descriptionFrame.Left = ShapePosition.Left;
            //descriptionFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            //descriptionFrame.Top = "9.0cm";
            //descriptionFrame.RelativeVertical = RelativeVertical.Page;
            //descriptionFrame.LineFormat.Width = "0.1mm";

            Table tableDescription = section.AddTable();

            tableDescription.Borders.Color = Color.Empty;
            tableDescription.Borders.Width = 0.25;
            tableDescription.Borders.Left.Width = 0.5;
            tableDescription.Borders.Right.Width = 0.5;
            tableDescription.Rows.LeftIndent = 0;

            tableDescription.Style = "Table";
            //this.table.Borders.Color = TableBorder;
            //this.table.Borders.Width = 0.25;
            //this.table.Borders.Left.Width = 0.5;
            //this.table.Borders.Right.Width = 0.5;
            tableDescription.Rows.LeftIndent = 0;

            // Prvo moraju da se naprave kolone
            column = tableDescription.AddColumn("14cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            Column column2 = tableDescription.AddColumn("5cm");
            column2.Format.Alignment = ParagraphAlignment.Center;


            row = tableDescription.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;

            Paragraph paragraphDescription = row.Cells[0].AddParagraph();
            paragraphDescription.Format.Font.Name = "Times New Roman";
            paragraphDescription.Format.Font.Size = 10;
            paragraphDescription.AddText(@"Опис");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            paragraphDescription = row.Cells[1].AddParagraph();
            paragraphDescription.Format.Font.Name = "Times New Roman";
            paragraphDescription.Format.Font.Size = 10;
            paragraphDescription.AddText(@"Износ");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;


            row = tableDescription.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Justify;
            row.Format.Font.Bold = false;
            
            paragraphDescription = row.Cells[0].AddParagraph();
            paragraphDescription.Format.Font.Name = "Times New Roman";
            paragraphDescription.Format.Font.Size = 10;
            paragraphDescription.AddText(@"За фактурисане материјалне и друге зависне трошкове исхране, смештаја и васпитног рада ученика за ");
            paragraphDescription.AddFormattedText(InvoiceTitleData.InvoiceForMonth + " " + InvoiceTitleData.InvoiceForYear, TextFormat.Bold);
            paragraphDescription.AddText(@" године у установама ученичког стандарда");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Justify;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            
            paragraphDescription = row.Cells[0].AddParagraph();
            paragraphDescription.Format.Font.Name = "Times New Roman";
            paragraphDescription.Format.Font.Size = 10;
            paragraphDescription.AddText(@"     Плаћање се врши на основу члана 23. Закона о државној управи (""Службени гласник РС"" бр. 79/05, 101/07, 95/10, 99/14, 30/18 и 47/18), члана 85. Закона о ученичком и студентском стандарду (""Службени гласник РС"", број 18/10, 55/13, 27/18 и 10/19), члана 8. Закона о буџету Републике Србије за 2021. годину (""Службени гласник РС"", бр. 149/20), Решењем о расписивању конкурса за пријем ученика средњих школа у Републици Србији у установе за смештај и исхрану ученика за школску 2020/2021.годину број: 451-02-1263/2020-05 од 25.5.2020. године,, односно на бази попуњених капацитета и реализованих услуга исхране ученика и Решењем Министарства просвете, науке и технолошког развоја број 451-02-167/2017-05 од 24.1.2017. године ");
            //paragraphDescription.AddText(@"Плаћање се врши на основу члана 23. Закона о државној управи (""Службени гласник РС"" бр. 79/05 и 101/07), члана 85. Закона о ученичком и студентском стандарду (""Службени гласник РС"" бр. 18/10 и 55/13), члана 8. Закона о буџету Републике Србије за 2016. годину (""Службени гласник РС"" бр. 103/15) и Решења Министарства просвете, науке и технолошког развоја број 451-02-2462/2013-05 од 22.08.2013. године");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Justify;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            paragraphDescription = row.Cells[1].AddParagraph();
            paragraphDescription.Format.Font.Name = "Times New Roman";
            paragraphDescription.Format.Font.Size = 10;
            paragraphDescription.AddFormattedText(InvoiceContent.Total, TextFormat.Bold);
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Top;


            section.AddParagraph();

            //////////////////////////////////////////////
            //   OPIS ISPLATE
            //////////////////////////////////////////////
            //TextFrame descriptionPaymentFrame;
            //descriptionPaymentFrame = section.AddTextFrame();
            //descriptionPaymentFrame.Height = "4cm";
            //descriptionPaymentFrame.Width = "16.0cm";
            //descriptionPaymentFrame.Left = ShapePosition.Right;
            //descriptionPaymentFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            //descriptionPaymentFrame.Top = "14.0cm";
            //descriptionPaymentFrame.RelativeVertical = RelativeVertical.Page;
            //descriptionFrame.LineFormat.Width = "0.1mm";


            Paragraph paragraphDescriptionPayment = section.AddParagraph();
            //paragraphReceiver = this.receiverFrame.AddParagraph();
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            paragraphDescriptionPayment.Format.SpaceAfter = 3;


            paragraphDescriptionPayment.AddText(@"Средства исплатити са:");
            paragraphDescriptionPayment.AddLineBreak();


            Table tableDescriptionPayment = section.AddTable();

            tableDescriptionPayment.Borders.Color = Color.Empty;
            tableDescriptionPayment.Borders.Width = 0.25;
            tableDescriptionPayment.Borders.Left.Width = 0.5;
            tableDescriptionPayment.Borders.Right.Width = 0.5;
            tableDescriptionPayment.Rows.LeftIndent = 0;

            tableDescriptionPayment.Style = "Table";
            //this.table.Borders.Color = TableBorder;
            //this.table.Borders.Width = 0.25;
            //this.table.Borders.Left.Width = 0.5;
            //this.table.Borders.Right.Width = 0.5;
            tableDescriptionPayment.Rows.LeftIndent = "5cm";

            // Prvo moraju da se naprave kolone, odnosno ovde jedna kolona
            column = tableDescriptionPayment.AddColumn("14cm");
            column.Format.Alignment = ParagraphAlignment.Right;


            //row = tableDescriptionPayment.AddRow();
            //row.HeadingFormat = true;
            //row.Format.Alignment = ParagraphAlignment.Center;
            //row.Format.Font.Bold = false;

            //paragraphDescriptionPayment = row.Cells[0].AddParagraph();
            //paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            //paragraphDescriptionPayment.Format.Font.Size = 10;
            //paragraphDescriptionPayment.AddText(@"Средства исплатити са:");
            //row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            //row.Cells[0].VerticalAlignment = VerticalAlignment.Center;


            row = tableDescriptionPayment.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Justify;
            row.Format.Font.Bold = false;

            paragraphDescriptionPayment = row.Cells[0].AddParagraph();
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            paragraphDescriptionPayment.AddText(@"- Програма:");
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddText(@"  2007 - Подршка у образовању ученика и студената");
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddText(@"- Програмска активност:");
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddText(@"  0001 - Систем установа ученичког стандарда");
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddText(@"  Извор: - 01");
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddText(@"- Економ. класификација:");
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddText(@"  494727 - Накнаде за социјалну заштиту из буџета");

            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;


            section.AddParagraph();

            //////////////////////////////////////////////
            //   TABELA DOMOVA
            //////////////////////////////////////////////
            //bilo koji od dva naredna reda
            //section.AddPageBreak();
            //section = this.document.AddSection();

            //invoiceFrame = section.AddTextFrame();
            //invoiceFrame.Height = "10cm";
            //invoiceFrame.Width = "16.0cm";
            //invoiceFrame.Left = ShapePosition.Left;
            //invoiceFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            //invoiceFrame.Top = "18.0cm";
            //invoiceFrame.RelativeVertical = RelativeVertical.Page;
            //            this.invoiceFrame.LineFormat.Width = "0.1mm";


            Paragraph paragraphInvoiceDescription = section.AddParagraph();
//            Paragraph paragraphInvoiceDescription = invoiceFrame.AddParagraph();
            //paragraphReceiver = this.receiverFrame.AddParagraph();
            paragraphInvoiceDescription.Format.Font.Name = "Times New Roman";
            paragraphInvoiceDescription.Format.Font.Size = 10;
            paragraphInvoiceDescription.Format.SpaceAfter = 3;


            paragraphInvoiceDescription.AddText(@"Добављач/поверилац");
            paragraphInvoiceDescription.AddLineBreak();


            tableInvoice = section.AddTable();

            //this.tableInvoice = section.AddTable();
            //            this.tableInvoice.Style = "Table";
            //this.tableInvoice.Borders.Color = TableBorder;
            this.tableInvoice.Borders.Color = Color.Empty;
            this.tableInvoice.Borders.Width = 0.25;
            this.tableInvoice.Borders.Left.Width = 0.5;
            this.tableInvoice.Borders.Right.Width = 0.5;
            this.tableInvoice.Rows.LeftIndent = 0;

            List<Paragraph> paragraphInvoices = new List<Paragraph>();

            // Before you can add a row, you must define the columns
            column = this.tableInvoice.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.tableInvoice.AddColumn("8.5cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = this.tableInvoice.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = this.tableInvoice.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = this.tableInvoice.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.tableInvoice.AddColumn("2.5cm");
            column.Format.Alignment = ParagraphAlignment.Right;



            paragraphInvoiceTitle.Format.Font.Name = "Times New Roman";
            paragraphInvoiceTitle.Format.Font.Size = 10;


            // Create the header of the table
            row = tableInvoice.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;

            paragraphInvoices.Add(row.Cells[0].AddParagraph("Р. бр."));
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].Format.Font.Name = "Times New Roman";
            row.Cells[0].Format.Font.Size = 10;
            //row.Cells[0].MergeDown = 1;

            paragraphInvoices.Add(row.Cells[1].AddParagraph("Назив установе"));
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].Format.Font.Name = "Times New Roman";
            row.Cells[1].Format.Font.Size = 10;

            paragraphInvoices.Add(row.Cells[2].AddParagraph("Број рачуна"));
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].Format.Font.Name = "Times New Roman";
            row.Cells[2].Format.Font.Size = 10;
            //row.Cells[3].MergeRight = 1;

            paragraphInvoices.Add(row.Cells[3].AddParagraph("Број фактуре"));
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].Format.Font.Name = "Times New Roman";
            row.Cells[3].Format.Font.Size = 10;

            paragraphInvoices.Add(row.Cells[4].AddParagraph("Датум фактуре"));
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[4].Format.Font.Name = "Times New Roman";
            row.Cells[4].Format.Font.Size = 10;
            //row.Cells[5].MergeDown = 1;

            paragraphInvoices.Add(row.Cells[5].AddParagraph("Износ"));
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[5].Format.Font.Name = "Times New Roman";
            row.Cells[5].Format.Font.Size = 10;

            //prolazak kroz sve clanove liste domova
            int cnt = 0;
            foreach (var dom in InvoiceContent.ListaDomovaZaFakturu)
            {
                

                row = tableInvoice.AddRow();
                row.HeadingFormat = false;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Bold = false;

                paragraphInvoices.Add(row.Cells[0].AddParagraph((++cnt).ToString()+"."));
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[0].Format.Font.Name = "Times New Roman";
                row.Cells[0].Format.Font.Size = 10;
                //row.Cells[0].MergeDown = 1;

                paragraphInvoices.Add(row.Cells[1].AddParagraph(dom.ImeDoma));
                row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[1].Format.Font.Name = "Times New Roman";
                row.Cells[1].Format.Font.Size = 10;

                paragraphInvoices.Add(row.Cells[2].AddParagraph(dom.BrojRacuna));
                row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[2].Format.Font.Name = "Times New Roman";
                row.Cells[2].Format.Font.Size = 10;
                //row.Cells[3].MergeRight = 1;

                paragraphInvoices.Add(row.Cells[3].AddParagraph(dom.BrojFakture));
                row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[3].Format.Font.Name = "Times New Roman";
                row.Cells[3].Format.Font.Size = 10;

                paragraphInvoices.Add(row.Cells[4].AddParagraph(dom.DatumFakture));
                row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[4].Format.Font.Name = "Times New Roman";
                row.Cells[4].Format.Font.Size = 10;
                //row.Cells[5].MergeDown = 1;

                paragraphInvoices.Add(row.Cells[5].AddParagraph(dom.Total));
                row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[5].Format.Font.Name = "Times New Roman";
                row.Cells[5].Format.Font.Size = 10;
            }


            //////////////////////////////////////////////
            //   NAČIN PLAĆANJA
            //////////////////////////////////////////////


            paragraphDescriptionPayment = section.AddParagraph();
            //paragraphReceiver = this.receiverFrame.AddParagraph();
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            paragraphDescriptionPayment.Format.SpaceAfter = 3;

            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddText(@"Начин плаћања: ");
            paragraphDescriptionPayment.AddFormattedText(@"месечно", TextFormat.Bold);
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddLineBreak();

            paragraphDescriptionPayment.AddText(@"Документација у прилогу: ");
            paragraphDescriptionPayment.AddFormattedText(@"рачуни", TextFormat.Bold);
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddLineBreak();


            tableDescriptionPayment = section.AddTable();

            tableDescriptionPayment.Style = "Table";
            //this.table.Borders.Color = TableBorder;
            //this.table.Borders.Width = 0.25;
            //this.table.Borders.Left.Width = 0.5;
            //this.table.Borders.Right.Width = 0.5;
            //tableDescriptionPayment.Rows.LeftIndent = "5cm";

            // Prvo moraju da se naprave kolone, odnosno ovde jedna kolona
            column = tableDescriptionPayment.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = tableDescriptionPayment.AddColumn("18cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            row = tableDescriptionPayment.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;


            paragraphDescriptionPayment = row.Cells[0].AddParagraph("Установе су испуниле обавезе према Министарству просвете, науке и технолошког развоја, у складу са:");
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Justify;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].MergeRight = 1;

            row = tableDescriptionPayment.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;

            paragraphDescriptionPayment = row.Cells[0].AddParagraph("-");
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Top;
            
            paragraphDescriptionPayment = row.Cells[1].AddParagraph(@"Решењем о расписивању конкурса за пријем ученика средњих школа у Републици Србији у установе за смештај и исхрану ученика за школску 2020/2021.годину број: 451-02-1263/2020-05 од 25.5.2020. године,, односно на бази попуњених капацитета и реализованих услуга");
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[1].Format.Alignment = ParagraphAlignment.Justify;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;

            row = tableDescriptionPayment.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;

            paragraphDescriptionPayment = row.Cells[0].AddParagraph("-");
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Top;

            //stari tekst: paragraphDescriptionPayment = row.Cells[1].AddParagraph(@"Решењем о утврђивању цена смештаја и исхране ученика у ученичким домовима за ученике који се финансирају из буџета Републике Србије број 451-02-2462/2013-05 од 22.08.2013. године, утврђена је висина месечних цена услуга, као и део цене која се надокнађује из буџета Републике Србије.");
            paragraphDescriptionPayment = row.Cells[1].AddParagraph(@"Решењем о утврђивању цена смештаја и исхране ученика у ученичким домовима за ученике који се финансирају из буџета Републике Србије број 451-02-167/2017-05 од 24.01.2017. године утврђена је висина месечних цена услуга, као и део цене која се надокнађује из буџета Републике Србије.");
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[1].Format.Alignment = ParagraphAlignment.Justify;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            

            //row = tableDescriptionPayment.AddRow();
            //row.HeadingFormat = true;
            //row.Format.Alignment = ParagraphAlignment.Center;
            //row.Format.Font.Bold = false;

            //paragraphDescriptionPayment = row.Cells[0].AddParagraph();
            //paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            //paragraphDescriptionPayment.Format.Font.Size = 10;
            //paragraphDescriptionPayment.AddText(@"Средства исплатити са:");
            //row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            //row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            section.AddParagraph();
            section.AddParagraph();

            //////////////////////////////////////////////
            //   POTPISNICI
            //////////////////////////////////////////////

            tableDescriptionPayment = section.AddTable();

            tableDescriptionPayment.Style = "Table";
            //this.table.Borders.Color = TableBorder;
            //this.table.Borders.Width = 0.25;
            //this.table.Borders.Left.Width = 0.5;
            //this.table.Borders.Right.Width = 0.5;
            //tableDescriptionPayment.Rows.LeftIndent = "5cm";

            column = tableDescriptionPayment.AddColumn("7cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = tableDescriptionPayment.AddColumn("5cm");
            
            column = tableDescriptionPayment.AddColumn("7cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            row = tableDescriptionPayment.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;


            //signer 4 je naknadno dodat iznad svih ostalih potpisnika (Lice koje je pripremilo dokument)
            paragraphDescriptionPayment = row.Cells[0].AddParagraph(Signer4.Description);
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment = row.Cells[0].AddParagraph("потпис _________________________");
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            paragraphDescriptionPayment = row.Cells[0].AddParagraph(Signer4.Name);
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;



            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddLineBreak();

            row = tableDescriptionPayment.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;


            paragraphDescriptionPayment = row.Cells[0].AddParagraph(Signer1.Description);
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment = row.Cells[0].AddParagraph("потпис _________________________");
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            paragraphDescriptionPayment = row.Cells[0].AddParagraph(Signer1.Name);
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddLineBreak();
            
            row = tableDescriptionPayment.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            
            paragraphDescriptionPayment = row.Cells[0].AddParagraph(Signer2.Description);
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment = row.Cells[0].AddParagraph("потпис _________________________");
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            paragraphDescriptionPayment = row.Cells[0].AddParagraph(Signer2.Name);
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;


            row = tableDescriptionPayment.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;

            paragraphDescriptionPayment = row.Cells[2].AddParagraph(SignerData.Description);
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;

            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment.AddLineBreak();
            paragraphDescriptionPayment = row.Cells[2].AddParagraph("потпис _________________________");
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;

            paragraphDescriptionPayment = row.Cells[2].AddParagraph(SignerData.Name + ", " + SignerData.Function);
            paragraphDescriptionPayment.Format.Font.Name = "Times New Roman";
            paragraphDescriptionPayment.Format.Font.Size = 10;
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;






            section.AddParagraph();
          

           
            return this.document;
        }
    }
}
