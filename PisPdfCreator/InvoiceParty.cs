﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisPdfCreator
{
    public class InvoiceParty
    {
        public string Name { get; set; }
        public string NameSup { get; set; }
        public string NameSub { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string FaxNumber { get; set; }
        public string RegistrationNumber { get; set; }
        public string TaxNumber { get; set; }
        public string AccountNumber { get; set; }
        public string Logo { get; set; }

        public InvoiceParty(string name, string nameSup, string nameSub, string address, string zipCode, string city, string phoneNumber1, string phoneNumber2, string faxNumber, string registrationNumber, string taxNumber, string accountNumber)
        {
            Name = name;
            NameSup = nameSup;
            NameSub = nameSub;
            Address = address;
            ZipCode = zipCode;
            City = city;
            PhoneNumber1 = phoneNumber1;
            PhoneNumber2 = phoneNumber2;
            FaxNumber = faxNumber;
            RegistrationNumber = registrationNumber;
            TaxNumber = taxNumber;
            AccountNumber = accountNumber;
        }

        public InvoiceParty(string name, string nameSup, string nameSub, string address, string zipCode, string city, string phoneNumber1, string phoneNumber2, string faxNumber, string registrationNumber, string taxNumber, string accountNumber, string logo)
        {
            Name = name;
            NameSup = nameSup;
            NameSub = nameSub;
            Address = address;
            ZipCode = zipCode;
            City = city;
            PhoneNumber1 = phoneNumber1;
            PhoneNumber2 = phoneNumber2;
            FaxNumber = faxNumber;
            RegistrationNumber = registrationNumber;
            TaxNumber = taxNumber;
            AccountNumber = accountNumber;
            Logo = logo;
        }

    }
}
