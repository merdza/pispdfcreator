﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisPdfCreator
{
    public class InvoiceAnexData
    {
        public string InvoicePeriod { get; set; }
        public string GraphicSignatureImagePath { get; set; }
        public List<InvoiceAnexDataRow> InvoiceAnexDataRows { get; set; }

        public InvoiceAnexData()
        {

        }
    }
}
