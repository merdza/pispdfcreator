﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisPdfCreator
{
    public class InvoiceMetaDataDomoviUcenika : InvoiceMetaData
    {
        public string InvoiceForMonth { get; set; }
        public string InvoiceForYear { get; set; }
        public string Note { get; set; }

        public InvoiceMetaDataDomoviUcenika(string invoiceNumber, string invoiceDate, string invoicePlaceOfIssuance, string invoiceForMonth, string invoiceForYear, string paymentTerms, string note)
            : base(invoiceNumber, invoiceDate, invoicePlaceOfIssuance, paymentTerms) 
        {
            InvoiceForMonth = invoiceForMonth;
            InvoiceForYear = invoiceForYear;
            Note = note;
        }
    }
}
