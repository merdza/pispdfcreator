﻿using System;
using System.Collections.Generic;
using System.Text;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;
using System.Xml;
using System.Xml.XPath;
using System.Globalization;
using System.Diagnostics;

namespace PisPdfCreator
{
    public abstract class Invoice
    {
        /// <summary>
        /// The MigraDoc document that represents the invoice.
        /// </summary>
        protected Document document;
        public DocumentMetaData DocumentMeta;
        public InvoiceParty Sender { get; set; }
        public InvoiceParty Receiver { get; set; }
        public InvoiceMetaData InvoiceTitleData { get; set; }
        public InvoiceData InvoiceContent { get; set; }
        public Signer SignerData { get; set; }
        public string GraphicSignatureImagePath { get; set; }

        public Invoice(DocumentMetaData documentMetaData, InvoiceParty sender, InvoiceParty receiver, InvoiceMetaData invoiceMeta, InvoiceData invoiceData, Signer signer)
        {
            DocumentMeta = documentMetaData;
            Sender = sender;
            Receiver = receiver;
            InvoiceTitleData = invoiceMeta;
            InvoiceContent = invoiceData;
            SignerData = signer;
        }

        protected TextFrame senderFrame;
        protected TextFrame receiverFrame;
        protected TextFrame invoiceTitleFrame;
        protected Table tableInvoiceTitle;
        protected Table tableInvoice;
        protected TextFrame invoiceFrame;
        protected TextFrame notesFrame;
        protected TextFrame signerFrame;
        protected TextFrame legalFrameFrame;
        protected TextFrame footerFrame;

        protected Table tableAnexTitle;
        protected TextFrame anexListFrame;
        protected Table tableAnex;

        public abstract Document CreateDocument();

        public virtual void DefineStyles()
        {
            // Get the predefined style Normal.
            Style style = this.document.Styles["Normal"];
            // Because all styles are derived from Normal, the next line changes the 
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = "Verdana";

            style = this.document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = this.document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

            // Create a new style called Table based on style Normal
            style = this.document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = "Verdana";
            style.Font.Name = "Times New Roman";
            style.Font.Size = 9;

            // Create a new style called Reference based on style Normal
            style = this.document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }

        // Some pre-defined colors
#if true
        // RGB colors
        protected readonly static Color TableBorder = new Color(81, 125, 192);
        protected readonly static Color TableBlue = new Color(235, 240, 249);
        protected readonly static Color TableGray = new Color(242, 242, 242);
#else
    // CMYK colors
    readonly static Color tableBorder = Color.FromCmyk(100, 50, 0, 30);
    readonly static Color tableBlue = Color.FromCmyk(0, 80, 50, 30);
    readonly static Color tableGray = Color.FromCmyk(30, 0, 0, 0, 100);
#endif
    }
}
