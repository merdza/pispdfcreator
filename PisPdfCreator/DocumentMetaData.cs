﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisPdfCreator
{
    public class DocumentMetaData
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Subject { get; set; }
        public string Keywords { get; set; }

        public DocumentMetaData(string title, string author, string subject, string keywords)
        {
            Title = title;
            Author = author;
            Subject = subject;
            Keywords = keywords;
        }
    }
}
