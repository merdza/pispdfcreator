﻿using System;
using System.Collections.Generic;
using System.Text;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;
using System.Xml.XPath;
using System.Linq;

namespace PisPdfCreator
{
    public class InvoiceDomoviUcenika : Invoice
    {
        public new InvoiceMetaDataDomoviUcenika InvoiceTitleData { get; set; }
        public new InvoiceDataDomoviUcenika InvoiceContent { get; set; }
        public InvoiceAnexData InvoiceAnexData { get; set; }

        public InvoiceDomoviUcenika(DocumentMetaData documentMetaData, InvoiceParty sender, InvoiceParty receiver, InvoiceMetaData invoiceTitleData, InvoiceData invoiceData, Signer signer, InvoiceAnexData invoiceAnexData)
            : base(documentMetaData, sender, receiver, invoiceTitleData, invoiceData, signer)
        {
            InvoiceTitleData = (InvoiceMetaDataDomoviUcenika)invoiceTitleData;
            InvoiceContent = (InvoiceDataDomoviUcenika)invoiceData;
            InvoiceAnexData = invoiceAnexData;
        }


        public override Document CreateDocument()
        {
            this.document = new Document();
            // Each MigraDoc document needs at least one section.
            Section section = this.document.AddSection();
            Column column;
            Row row;





            /////////////////////////////////////////////////////////////
            /// Nova stranica za novi izvestaj - Anex
            /////////////////////////////////////////////////////////////
            


            this.senderFrame = section.AddTextFrame();
            this.senderFrame.Height = "2cm";
            this.senderFrame.Width = "13.0cm";
            this.senderFrame.Left = ShapePosition.Left;
            this.senderFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.senderFrame.Top = "2.0cm";
            this.senderFrame.RelativeVertical = RelativeVertical.Page;

            Paragraph paragraphIssuer = senderFrame.AddParagraph();
            //paragraphSender = this.senderFrame.AddParagraph();
            paragraphIssuer.Format.Font.Name = "Times New Roman";
            paragraphIssuer.Format.Font.Size = 10;
            paragraphIssuer.AddText(Sender.Name);



            //datum izdavanja
            this.senderFrame = section.AddTextFrame();
            this.senderFrame.Height = "1cm";
            this.senderFrame.Width = "3.0cm";
            this.senderFrame.Left = "14cm";
            this.senderFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.senderFrame.Top = "2.0cm";
            this.senderFrame.RelativeVertical = RelativeVertical.Page;
            Paragraph paragraphDate = senderFrame.AddParagraph();
            paragraphDate.Format.Font.Name = "Times New Roman";
            paragraphDate.Format.Font.Size = 10;
            paragraphDate.AddText(InvoiceTitleData.InvoiceDate);




            //////////////////////////////////////////////
            //   Pregled izdatih obroka
            //////////////////////////////////////////////
            this.invoiceTitleFrame = section.AddTextFrame();
            this.invoiceTitleFrame.Height = "2cm";
            this.invoiceTitleFrame.Width = "16.0cm";
            this.invoiceTitleFrame.Left = ShapePosition.Right;
            this.invoiceTitleFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.invoiceTitleFrame.Top = "3.0cm";
            this.invoiceTitleFrame.RelativeVertical = RelativeVertical.Page;
            //this.invoiceTitleFrame.LineFormat.Width = "0.1mm";

            //Naslov racuna ce se ispisati u tabeli sa jednim redom i jednom kolonom
            this.tableAnexTitle = invoiceTitleFrame.AddTable();

            this.tableAnexTitle.Style = "Table";
            this.tableAnexTitle.Rows.LeftIndent = 0;

            // Prvo moraju da se naprave kolone, odnosno ovde jedna kolona
            column = this.tableAnexTitle.AddColumn("16cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Prvi red u tabeli
            row = tableAnexTitle.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            //row.Shading.Color = TableBlue;
            Paragraph paragraphAnexTitle = row.Cells[0].AddParagraph();
            paragraphAnexTitle.Format.Font.Name = "Times New Roman";
            paragraphAnexTitle.Format.Font.Size = 14;
            paragraphAnexTitle.Format.Font.Bold = true;
            paragraphAnexTitle.AddText("ПРЕГЛЕД ИЗДАТИХ ОБРОКА");



            //row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;


            //period
            row = tableAnexTitle.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            //row.Shading.Color = TableBlue;
            paragraphAnexTitle = row.Cells[0].AddParagraph();
            paragraphAnexTitle.Format.Font.Name = "Times New Roman";
            paragraphAnexTitle.Format.Font.Size = 12;
            paragraphAnexTitle.AddText(InvoiceAnexData.InvoicePeriod);
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;


            //tip korisnika
            row = tableAnexTitle.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            //row.Shading.Color = TableBlue;
            paragraphAnexTitle = row.Cells[0].AddParagraph();
            paragraphAnexTitle.Format.Font.Name = "Times New Roman";
            paragraphAnexTitle.Format.Font.Size = 12;
            paragraphAnexTitle.AddText("УЧЕНИЦИ");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;



            //////////////////////////////////////////////
            //   TABELA
            //////////////////////////////////////////////
            this.anexListFrame = section.AddTextFrame();
            this.anexListFrame.Height = "22cm";
            this.anexListFrame.Width = "16.0cm";
            this.anexListFrame.Left = ShapePosition.Right;
            this.anexListFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.anexListFrame.Top = "5.0cm";
            this.anexListFrame.RelativeVertical = RelativeVertical.Page;
            //            this.anexListFrame.LineFormat.Width = "0.1mm";

            this.tableAnex = anexListFrame.AddTable();

            //this.tableAnex = section.AddTable();
            //            this.tableAnex.Style = "Table";
            //this.tableAnex.Borders.Color = TableBorder;
            this.tableAnex.Borders.Color = Color.Empty;
            this.tableAnex.Borders.Width = 0.25;
            this.tableAnex.Borders.Left.Width = 0.5;
            this.tableAnex.Borders.Right.Width = 0.5;
            this.tableAnex.Rows.LeftIndent = 0;

            List<Paragraph> paragraphAnexList = new List<Paragraph>();

            // Before you can add a row, you must define the columns
            column = this.tableAnex.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.tableAnex.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.tableAnex.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.tableAnex.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.tableAnex.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.tableAnex.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Center;


            foreach (var invoiceAnexDataRow in InvoiceAnexData.InvoiceAnexDataRows)
            {
                row = tableAnex.AddRow();
                row.Height = "0.5cm";
                if (invoiceAnexDataRow.Equals(InvoiceAnexData.InvoiceAnexDataRows.First()))
                {
                    // Create the header of the table

                    row.HeadingFormat = true;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.Format.Font.Bold = true;
                }
                else if (invoiceAnexDataRow.Equals(InvoiceAnexData.InvoiceAnexDataRows.Last()))
                {
                    row.HeadingFormat = false;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.Format.Font.Bold = true;
                }
                else
                {
                    row.HeadingFormat = false;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.Format.Font.Bold = false;
                }

                paragraphAnexList.Add(row.Cells[0].AddParagraph(invoiceAnexDataRow.Column0));
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

                paragraphAnexList.Add(row.Cells[1].AddParagraph(invoiceAnexDataRow.Column1));
                row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;

                paragraphAnexList.Add(row.Cells[2].AddParagraph(invoiceAnexDataRow.Column2));
                row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[2].VerticalAlignment = VerticalAlignment.Center;

                paragraphAnexList.Add(row.Cells[3].AddParagraph(invoiceAnexDataRow.Column3));
                row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[3].VerticalAlignment = VerticalAlignment.Center;

                paragraphAnexList.Add(row.Cells[4].AddParagraph(invoiceAnexDataRow.Column4));
                row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[4].VerticalAlignment = VerticalAlignment.Center;

                paragraphAnexList.Add(row.Cells[5].AddParagraph(invoiceAnexDataRow.Column5));
                row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[5].VerticalAlignment = VerticalAlignment.Center;

            }

            foreach (Paragraph paragraph in paragraphAnexList)
            {
                paragraph.Format.Font.Name = "Times New Roman";
                paragraph.Format.Font.Size = 10;
            }

            row = tableAnex.AddRow();
            row.Cells[0].MergeRight = 5;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[0].Borders.Bottom.Visible = false;
            row.Cells[0].Borders.Left.Visible = false;
            row.Cells[5].Borders.Right.Visible = false;

            Image image = row.Cells[0].AddParagraph().AddImage(InvoiceAnexData.GraphicSignatureImagePath);
            image.Width = "10cm";








            section.AddPageBreak();







            //////////////////////////////////////////////
            //   POŠILJALAC
            //////////////////////////////////////////////
            this.senderFrame = section.AddTextFrame();
            this.senderFrame.Height = "4.5cm";
            this.senderFrame.Width = "7.0cm";
            this.senderFrame.Left = ShapePosition.Left;
            this.senderFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.senderFrame.Top = "2.0cm";
            this.senderFrame.RelativeVertical = RelativeVertical.Page;
            //this.senderFrame.LineFormat.Width = "0.1mm";

            //podaci posiljaoca
            Paragraph paragraphSender = senderFrame.AddParagraph();
            //paragraphSender = this.senderFrame.AddParagraph();
            paragraphSender.Format.Font.Name = "Times New Roman";
            paragraphSender.Format.Font.Size = 7;
            paragraphSender.Format.SpaceAfter = 3;

            Paragraph paragraphSenderName = senderFrame.AddParagraph();
            //paragraphSenderName = this.senderFrame.AddParagraph();
            paragraphSenderName.Format.Font.Name = "Times New Roman";
            paragraphSenderName.Format.Font.Size = 10;
            paragraphSenderName.Format.SpaceAfter = 3;
            paragraphSenderName.Format.Font.Bold = true;

            Paragraph paragraphSenderData = senderFrame.AddParagraph();
            //paragraphSenderData = this.senderFrame.AddParagraph();
            paragraphSenderData.Format.Font.Name = "Times New Roman";
            paragraphSenderData.Format.Font.Size = 10;
            paragraphSenderData.Format.SpaceAfter = 3;

            //            paragraph.AddLineBreak();
            paragraphSender.AddText("Издавалац рачуна:");
            paragraphSender.AddLineBreak();
            paragraphSenderName.AddText(Sender.Name);
            // paragraph.AddLineBreak();
            paragraphSenderData.AddText(Sender.Address);
            paragraphSenderData.AddLineBreak();
            paragraphSenderData.AddText(Sender.ZipCode + " " + Sender.City);
            paragraphSenderData.AddLineBreak();
            paragraphSenderData.AddText(Sender.PhoneNumber1);
            paragraphSenderData.AddLineBreak();
            paragraphSenderData.AddText(@"Матични број: " + Sender.RegistrationNumber);
            paragraphSenderData.AddLineBreak();
            paragraphSenderData.AddText(@"ПИБ: " + Sender.TaxNumber);
            paragraphSenderData.AddLineBreak();
            paragraphSenderData.AddText(@"Рачун број: " + Sender.AccountNumber);
            paragraphSenderData.AddLineBreak();



            //////////////////////////////////////////////
            //   PRIMALAC
            //////////////////////////////////////////////
            this.receiverFrame = section.AddTextFrame();
            this.receiverFrame.Height = "4.5cm";
            this.receiverFrame.Width = "7.0cm";
            this.receiverFrame.Left = ShapePosition.Right;
            this.receiverFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.receiverFrame.Top = "2.0cm";
            this.receiverFrame.RelativeVertical = RelativeVertical.Page;
            //this.receiverFrame.LineFormat.Width = "0.1mm";

            //podaci primaoca
            Paragraph paragraphReceiver = receiverFrame.AddParagraph();
            paragraphReceiver.Format.Font.Name = "Times New Roman";
            paragraphReceiver.Format.Font.Size = 7;
            paragraphReceiver.Format.SpaceAfter = 3;

            Paragraph paragraphReceiverName = receiverFrame.AddParagraph();
            //paragraphReceiverName = this.receiverFrame.AddParagraph();
            paragraphReceiverName.Format.Font.Name = "Times New Roman";
            paragraphReceiverName.Format.Font.Size = 10;
            paragraphReceiverName.Format.SpaceAfter = 3;
            paragraphReceiverName.Format.Font.Bold = true;

            Paragraph paragraphReceiverData = receiverFrame.AddParagraph();
            //paragraphReceiverData = this.receiverFrame.AddParagraph();
            paragraphReceiverData.Format.Font.Name = "Times New Roman";
            paragraphReceiverData.Format.Font.Size = 10;
            paragraphReceiverData.Format.SpaceAfter = 3;

            paragraphReceiver.AddText("Прималац рачуна:");
            paragraphReceiver.AddLineBreak();
            paragraphReceiverName.AddText(Receiver.Name);
            paragraphReceiverName.AddLineBreak();
            paragraphReceiverName.AddText(Receiver.NameSub);
            paragraphReceiverData.AddText(Receiver.Address);
            paragraphReceiverData.AddLineBreak();
            paragraphReceiverData.AddText(Receiver.ZipCode + " " + Receiver.City);
            paragraphReceiverData.AddLineBreak();
            paragraphReceiverData.AddText(@"Матични број: " + Receiver.RegistrationNumber);
            paragraphReceiverData.AddLineBreak();
            paragraphReceiverData.AddText(@"ПИБ: " + Receiver.TaxNumber);
            paragraphReceiverData.AddLineBreak();



            //////////////////////////////////////////////
            //   RAČUN
            //////////////////////////////////////////////
            this.invoiceTitleFrame = section.AddTextFrame();
            this.invoiceTitleFrame.Height = "2cm";
            this.invoiceTitleFrame.Width = "16.0cm";
            this.invoiceTitleFrame.Left = ShapePosition.Right;
            this.invoiceTitleFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.invoiceTitleFrame.Top = "8.0cm";
            this.invoiceTitleFrame.RelativeVertical = RelativeVertical.Page;
            //this.invoiceTitleFrame.LineFormat.Width = "0.1mm";

            //Naslov racuna ce se ispisati u tabeli sa jednim redom i jednom kolonom
            this.tableInvoiceTitle = invoiceTitleFrame.AddTable();

            this.tableInvoiceTitle.Style = "Table";
            //this.table.Borders.Color = TableBorder;
            //this.table.Borders.Width = 0.25;
            //this.table.Borders.Left.Width = 0.5;
            //this.table.Borders.Right.Width = 0.5;
            this.tableInvoiceTitle.Rows.LeftIndent = 0;

            // Prvo moraju da se naprave kolone, odnosno ovde jedna kolona
            column = this.tableInvoiceTitle.AddColumn("16cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Prvi red u tabeli
            row = tableInvoiceTitle.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            //row.Shading.Color = TableBlue;
            Paragraph paragraphInvoiceTitle = row.Cells[0].AddParagraph();
            paragraphInvoiceTitle.Format.Font.Name = "Times New Roman";
            paragraphInvoiceTitle.Format.Font.Size = 14;
            paragraphInvoiceTitle.Format.Font.Bold = true;
            paragraphInvoiceTitle.AddText("РАЧУН БРОЈ: " + InvoiceTitleData.InvoiceNumber);


            //row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            this.tableInvoiceTitle.SetEdge(0, 0, 1, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);


            //mesec
            row = tableInvoiceTitle.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            //row.Shading.Color = TableBlue;
            paragraphInvoiceTitle = row.Cells[0].AddParagraph();
            paragraphInvoiceTitle.Format.Font.Name = "Times New Roman";
            paragraphInvoiceTitle.Format.Font.Size = 10;
            paragraphInvoiceTitle.AddText("Рачун за месец: " + InvoiceTitleData.InvoiceForMonth.ToUpper() + " " + InvoiceTitleData.InvoiceForYear);
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            //datum izdavanja
            row = tableInvoiceTitle.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            paragraphInvoiceTitle = row.Cells[0].AddParagraph();
            paragraphInvoiceTitle.Format.Font.Name = "Times New Roman";
            paragraphInvoiceTitle.Format.Font.Size = 10;
            paragraphInvoiceTitle.AddText("Датум издавања: " + InvoiceTitleData.InvoiceDate);
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            //mesto izdavanja
            row = tableInvoiceTitle.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            paragraphInvoiceTitle = row.Cells[0].AddParagraph();
            paragraphInvoiceTitle.Format.Font.Name = "Times New Roman";
            paragraphInvoiceTitle.Format.Font.Size = 10;
            paragraphInvoiceTitle.AddText("Место издавања: " + InvoiceTitleData.InvoicePlaceOfIssuance);
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;


            //////////////////////////////////////////////
            //   TABELA
            //////////////////////////////////////////////
            this.invoiceFrame = section.AddTextFrame();
            this.invoiceFrame.Height = "10cm";
            this.invoiceFrame.Width = "16.0cm";
            this.invoiceFrame.Left = ShapePosition.Right;
            this.invoiceFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.invoiceFrame.Top = "12.0cm";
            this.invoiceFrame.RelativeVertical = RelativeVertical.Page;
            //            this.invoiceFrame.LineFormat.Width = "0.1mm";

            this.tableInvoice = invoiceFrame.AddTable();

            //this.tableInvoice = section.AddTable();
            //            this.tableInvoice.Style = "Table";
            //this.tableInvoice.Borders.Color = TableBorder;
            this.tableInvoice.Borders.Color = Color.Empty;
            this.tableInvoice.Borders.Width = 0.25;
            this.tableInvoice.Borders.Left.Width = 0.5;
            this.tableInvoice.Borders.Right.Width = 0.5;
            this.tableInvoice.Rows.LeftIndent = 0;

            List<Paragraph> paragraphInvoices = new List<Paragraph>();

            // Before you can add a row, you must define the columns
            column = this.tableInvoice.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.tableInvoice.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = this.tableInvoice.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = this.tableInvoice.AddColumn("2.5cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = this.tableInvoice.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.tableInvoice.AddColumn("2.5cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = this.tableInvoice.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            // Create the header of the table
            row = tableInvoice.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;

            paragraphInvoices.Add(row.Cells[0].AddParagraph("Р. бр."));
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            //row.Cells[0].MergeDown = 1;

            paragraphInvoices.Add(row.Cells[1].AddParagraph("Опис"));
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].MergeRight = 1;

            paragraphInvoices.Add(row.Cells[3].AddParagraph("Јединица"));
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            //row.Cells[3].MergeRight = 1;

            paragraphInvoices.Add(row.Cells[4].AddParagraph("Количина"));
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;

            paragraphInvoices.Add(row.Cells[5].AddParagraph("Јединична цена"));
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
            //row.Cells[5].MergeDown = 1;

            paragraphInvoices.Add(row.Cells[6].AddParagraph("Укупан износ"));
            row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[6].VerticalAlignment = VerticalAlignment.Center;

            //-------------------------------
            row = tableInvoice.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;

            paragraphInvoices.Add(row.Cells[0].AddParagraph("1."));
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].MergeDown = 3;

            paragraphInvoices.Add(row.Cells[1].AddParagraph("Исхрана ученика"));
            row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].MergeDown = 3;

            paragraphInvoices.Add(row.Cells[2].AddParagraph("Доручак"));
            row.Cells[2].Format.Alignment = ParagraphAlignment.Left;

            paragraphInvoices.Add(row.Cells[3].AddParagraph("Број оброка"));
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].MergeDown = 2;

            paragraphInvoices.Add(row.Cells[4].AddParagraph(InvoiceContent.BreakfastAmount));
            row.Cells[4].Format.Alignment = ParagraphAlignment.Right;

            paragraphInvoices.Add(row.Cells[5].AddParagraph(InvoiceContent.BreakfastUnitPrice));
            row.Cells[5].Format.Alignment = ParagraphAlignment.Right;

            paragraphInvoices.Add(row.Cells[6].AddParagraph(InvoiceContent.BreakfastTotalPrice));
            row.Cells[6].Format.Alignment = ParagraphAlignment.Right;


            //-------------------------------
            row = tableInvoice.AddRow();
            row.HeadingFormat = false;
            row.Format.Font.Bold = false;

            paragraphInvoices.Add(row.Cells[2].AddParagraph("Pучак"));
            row.Cells[2].Format.Alignment = ParagraphAlignment.Left;

            paragraphInvoices.Add(row.Cells[4].AddParagraph(InvoiceContent.LunchAmount));
            row.Cells[4].Format.Alignment = ParagraphAlignment.Right;

            paragraphInvoices.Add(row.Cells[5].AddParagraph(InvoiceContent.LunchUnitPrice));
            row.Cells[5].Format.Alignment = ParagraphAlignment.Right;

            paragraphInvoices.Add(row.Cells[6].AddParagraph(InvoiceContent.LunchTotalPrice));
            row.Cells[6].Format.Alignment = ParagraphAlignment.Right;

            //-------------------------------
            row = tableInvoice.AddRow();
            row.HeadingFormat = false;
            row.Format.Font.Bold = false;

            paragraphInvoices.Add(row.Cells[2].AddParagraph("Вечера"));
            row.Cells[2].Format.Alignment = ParagraphAlignment.Left;

            paragraphInvoices.Add(row.Cells[4].AddParagraph(InvoiceContent.DinnerAmount));
            row.Cells[4].Format.Alignment = ParagraphAlignment.Right;

            paragraphInvoices.Add(row.Cells[5].AddParagraph(InvoiceContent.DinnerUnitPrice));
            row.Cells[5].Format.Alignment = ParagraphAlignment.Right;

            paragraphInvoices.Add(row.Cells[6].AddParagraph(InvoiceContent.DinnerTotalPrice));
            row.Cells[6].Format.Alignment = ParagraphAlignment.Right;

            //-------------------------------
            row = tableInvoice.AddRow();
            row.HeadingFormat = false;
            //row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;

            paragraphInvoices.Add(row.Cells[2].AddParagraph("Укупно за исхрану ученика"));
            row.Cells[2].Format.Font.Bold = true;
            row.Cells[2].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[2].MergeRight = 3;

            paragraphInvoices.Add(row.Cells[6].AddParagraph(InvoiceContent.MealsTotalPrice));
            row.Cells[6].Format.Font.Bold = true;
            row.Cells[6].Format.Alignment = ParagraphAlignment.Right;


            //-------------------------------
            row = tableInvoice.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;

            paragraphInvoices.Add(row.Cells[0].AddParagraph("2."));
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].MergeDown = 1;

            paragraphInvoices.Add(row.Cells[1].AddParagraph("Смештај ученика"));
            row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].MergeDown = 1;
            row.Cells[1].MergeRight = 1;

            paragraphInvoices.Add(row.Cells[3].AddParagraph("Број ученика"));
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;

            paragraphInvoices.Add(row.Cells[4].AddParagraph(InvoiceContent.PupilsCount));
            row.Cells[4].Format.Alignment = ParagraphAlignment.Right;

            paragraphInvoices.Add(row.Cells[5].AddParagraph(InvoiceContent.AccommodationUnitPrice));
            row.Cells[5].Format.Alignment = ParagraphAlignment.Right;

            paragraphInvoices.Add(row.Cells[6].AddParagraph(InvoiceContent.AccommodationTotalPrice));
            row.Cells[6].Format.Alignment = ParagraphAlignment.Right;

            //-------------------------------
            row = tableInvoice.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;

            paragraphInvoices.Add(row.Cells[3].AddParagraph("Укупно за смештај ученика"));
            row.Cells[3].Format.Font.Bold = true;
            row.Cells[3].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].MergeRight = 2;

            paragraphInvoices.Add(row.Cells[6].AddParagraph(InvoiceContent.AccommodationTotalPrice));
            row.Cells[6].Format.Font.Bold = true;
            row.Cells[6].Format.Alignment = ParagraphAlignment.Right;

            //-------------------------------
            row = tableInvoice.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;

            paragraphInvoices.Add(row.Cells[0].AddParagraph("3."));
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].MergeDown = 1;

            paragraphInvoices.Add(row.Cells[1].AddParagraph("Васпитни рад"));
            row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].MergeDown = 1;
            row.Cells[1].MergeRight = 1;

            paragraphInvoices.Add(row.Cells[3].AddParagraph("Број ученика"));
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;

            paragraphInvoices.Add(row.Cells[4].AddParagraph(InvoiceContent.UpbringingCount));
            row.Cells[4].Format.Alignment = ParagraphAlignment.Right;

            paragraphInvoices.Add(row.Cells[5].AddParagraph(InvoiceContent.UpbringingUnitPrice));
            row.Cells[5].Format.Alignment = ParagraphAlignment.Right;

            paragraphInvoices.Add(row.Cells[6].AddParagraph(InvoiceContent.UpbringingTotalPrice));
            row.Cells[6].Format.Alignment = ParagraphAlignment.Right;

            //-------------------------------
            row = tableInvoice.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;

            paragraphInvoices.Add(row.Cells[3].AddParagraph("Укупно за васпитни рад"));
            row.Cells[3].Format.Font.Bold = true;
            row.Cells[3].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].MergeRight = 2;

            paragraphInvoices.Add(row.Cells[6].AddParagraph(InvoiceContent.UpbringingTotalPrice));
            row.Cells[6].Format.Font.Bold = true;
            row.Cells[6].Format.Alignment = ParagraphAlignment.Right;

            //-------------------------------
            row = tableInvoice.AddRow();
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;

            paragraphInvoices.Add(row.Cells[0].AddParagraph("УКУПНО 1 + 2 + 3"));
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].MergeRight = 5;

            paragraphInvoices.Add(row.Cells[6].AddParagraph(InvoiceContent.Total));
            row.Cells[6].Format.Font.Bold = true;
            row.Cells[6].Format.Alignment = ParagraphAlignment.Right;


            foreach (Paragraph paragraph in paragraphInvoices)
            {
                paragraph.Format.Font.Name = "Times New Roman";
                paragraph.Format.Font.Size = 10;
            }



            this.tableInvoice.SetEdge(0, 9, 7, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);


            //////////////////////////////////////////////
            //   ISPOD TABELE
            //////////////////////////////////////////////
            this.notesFrame = section.AddTextFrame();
            this.notesFrame.Height = "4cm";
            this.notesFrame.Width = "16.0cm";
            this.notesFrame.Left = ShapePosition.Left;
            this.notesFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.notesFrame.Top = "17.0cm";
            this.notesFrame.RelativeVertical = RelativeVertical.Page;
            //this.notesFrame.LineFormat.Width = "0.1mm";

            //podaci primaoca
            Paragraph paragraphNotes = notesFrame.AddParagraph();
            //paragraphReceiver = this.receiverFrame.AddParagraph();
            paragraphNotes.Format.Font.Name = "Times New Roman";
            paragraphNotes.Format.Font.Size = 10;
            paragraphNotes.Format.SpaceAfter = 3;


            paragraphNotes.AddText(@"Рок плаћања: " + InvoiceTitleData.PaymentTerms);
            paragraphNotes.AddLineBreak();
            paragraphNotes.AddLineBreak();
            paragraphNotes.AddText(@"Напомена: " + InvoiceTitleData.Note);
            paragraphNotes.AddLineBreak();

            //////////////////////////////////////////////
            //   POTPISNIK
            //////////////////////////////////////////////
            this.signerFrame = section.AddTextFrame();
            this.signerFrame.Height = "2cm";
            this.signerFrame.Width = "6.0cm";
            this.signerFrame.Left = ShapePosition.Left;
            this.signerFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.signerFrame.Top = "22.5cm";
            this.signerFrame.RelativeVertical = RelativeVertical.Page;
            //this.signerFrame.LineFormat.Width = "0.1mm";

            //podaci primaoca
            Paragraph paragraphSigner = signerFrame.AddParagraph();
            //paragraphReceiver = this.receiverFrame.AddParagraph();
            paragraphSigner.Format.Font.Name = "Times New Roman";
            paragraphSigner.Format.Font.Size = 10;
            paragraphSigner.Format.SpaceAfter = 3;


            paragraphSigner.AddText(SignerData.Description);
            paragraphSigner.AddLineBreak();
            paragraphSigner.AddText(SignerData.Name + ", " + SignerData.Function);
            //paragraphSigner.AddLineBreak();
            //paragraphSigner.AddText(SignerData.JobTitle);
            //ne treba mu linija jer se nece potpisivati rukom
            //paragraphSigner.AddLineBreak();
            //paragraphSigner.AddLineBreak();
            //paragraphSigner.AddText(@"____________________");
            //paragraphSigner.AddLineBreak();


            //////////////////////////////////////////////
            //   ZAKONSKI OKVIR
            //////////////////////////////////////////////
            this.legalFrameFrame = section.AddTextFrame();
            this.legalFrameFrame.Height = "4cm";
            this.legalFrameFrame.Width = "16.0cm";
            this.legalFrameFrame.Left = ShapePosition.Right;
            this.legalFrameFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.legalFrameFrame.Top = "26.5cm";
            this.legalFrameFrame.RelativeVertical = RelativeVertical.Page;
            //this.legalFrameFrame.LineFormat.Width = "0.1mm";

            //podaci primaoca
            Paragraph paragraphFooterNotes = legalFrameFrame.AddParagraph();
            //paragraphReceiver = this.receiverFrame.AddParagraph();
            paragraphFooterNotes.Format.Font.Name = "Times New Roman";
            paragraphFooterNotes.Format.Font.Size = 8;
            paragraphFooterNotes.Format.SpaceAfter = 3;
            paragraphFooterNotes.Format.Font.Italic = true;


            paragraphFooterNotes.AddText(@"Законски оквир:");
            paragraphFooterNotes.AddLineBreak();
            paragraphFooterNotes.AddText(@"Пуноважност и доказна снага електронског документа електронски потписаног важећим квалификованим електронским сертификатом је уређена Законом о електронском потпису Републике Србије („Службени гласник РС” бр. 135/04).");
            paragraphFooterNotes.AddLineBreak();
            paragraphFooterNotes.AddText(@"- Члан 3. Електронском документу се не може оспорити пуноважност или доказна снага само због тога што је у електронском облику.");
            paragraphFooterNotes.AddLineBreak();
            paragraphFooterNotes.AddText(@"- Члан 6. Електронски потпис може имати правно дејство и може се користити као доказно средство у законом уређеном поступку, осим када се, у складу са посебним законом захтева да само својеручни потпис има правно дејство и доказну снагу.");
            paragraphFooterNotes.AddLineBreak();


            //////////////////////////////////////////////
            //   FOOTER
            //////////////////////////////////////////////
            this.footerFrame = section.AddTextFrame();
            this.footerFrame.Height = "0.5cm";
            this.footerFrame.Width = "5.0cm";
            this.footerFrame.Left = ShapePosition.Right;
            this.footerFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.footerFrame.Top = "29.0cm";
            this.footerFrame.RelativeVertical = RelativeVertical.Page;
            //this.footerFrame.LineFormat.Width = "0.1mm";

            Paragraph paragraphFooterCreator = footerFrame.AddParagraph();
            //paragraphReceiver = this.receiverFrame.AddParagraph();
            paragraphFooterCreator.Format.Font.Name = "Times New Roman";
            paragraphFooterCreator.Format.Font.Size = 6;
            paragraphFooterCreator.Format.Font.Italic = true;
            paragraphFooterCreator.Format.Alignment = ParagraphAlignment.Right;


            paragraphFooterCreator.AddText(@"генерисано из Гаудеамус информационог система");




        


            
            return this.document;
        }
    }
}
