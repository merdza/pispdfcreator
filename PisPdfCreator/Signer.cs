﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisPdfCreator
{
    public class Signer
    {
        public string Name { get; set; } //ime potpisnika
        public string Function { get; set; } //funkcija potpisnika
        public string Description { get; set; } //tekst kojim se dodatno opisuje potpisnik

        public Signer(string name, string function, string descr = null)
        {
            Name = name;
            Function = function;
            Description = descr;
        }
    }
}
