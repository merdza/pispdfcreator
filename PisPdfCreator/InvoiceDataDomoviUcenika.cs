﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisPdfCreator
{
    public class InvoiceDataDomoviUcenika : InvoiceData
    {
        public string BreakfastAmount { get; set; }
        public string BreakfastUnitPrice { get; set; }
        public string BreakfastTotalPrice { get; set; }

        public string LunchAmount { get; set; }
        public string LunchUnitPrice { get; set; }
        public string LunchTotalPrice { get; set; }

        public string DinnerAmount { get; set; }
        public string DinnerUnitPrice { get; set; }
        public string DinnerTotalPrice { get; set; }

        public string MealsTotalPrice { get; set; }

        public string PupilsCount { get; set; }
        public string UpbringingCount { get; set; }
        public string AccommodationUnitPrice { get; set; }
        public string AccommodationTotalPrice { get; set; }

        public string UpbringingUnitPrice { get; set; }
        public string UpbringingTotalPrice { get; set; }

        public InvoiceDataDomoviUcenika(string breakfastAmount, string breakfastUnitPrice, string breakfastTotalPrice,
            string lunchAmount, string lunchUnitPrice, string lunchTotalPrice, 
            string dinnerAmount, string dinnerUnitPrice, string dinnerTotalPrice,
            string mealsTotalPrice, string pupilsCount, string upbringingCount, string accommodationUnitPrice, string accommodationTotalPrice, 
            string upbringingUnitPrice, string upbringingTotalPrice, string totalPrice)
            : base (totalPrice)
        {
            
            BreakfastAmount = breakfastAmount;
            BreakfastUnitPrice = breakfastUnitPrice;
            BreakfastTotalPrice = breakfastTotalPrice;

            LunchAmount = lunchAmount;
            LunchUnitPrice = lunchUnitPrice;
            LunchTotalPrice = lunchTotalPrice;

            DinnerAmount = dinnerAmount;
            DinnerUnitPrice = dinnerUnitPrice;
            DinnerTotalPrice = dinnerTotalPrice;

            MealsTotalPrice = mealsTotalPrice;

            PupilsCount = pupilsCount;
            UpbringingCount = upbringingCount;

            AccommodationUnitPrice = accommodationUnitPrice;
            AccommodationTotalPrice = accommodationTotalPrice;

            UpbringingUnitPrice = upbringingUnitPrice;
            UpbringingTotalPrice = upbringingTotalPrice;
        }
    }
}
