﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;



namespace PisPdfCreator
{
    public class PdfCreator
    {
        static double A4Width = XUnit.FromCentimeter(21).Point;
        static double A4Height = XUnit.FromCentimeter(29.7).Point;

        public static void CreatePdf(Invoice invoice, string pdfFileFullPath = "")
        {
            try
            {
                DateTime now = DateTime.Now;

                if (pdfFileFullPath.Equals(""))
                {
                    pdfFileFullPath = @"D:\IDCap\Ministarstvo racuni/Racun_";
                    pdfFileFullPath += Guid.NewGuid().ToString("D").ToUpper() + ".pdf";    
                }
                

                PdfDocument document = new PdfDocument();
                document.Info.Title = invoice.DocumentMeta.Title;
                document.Info.Author = invoice.DocumentMeta.Author;
                document.Info.Subject = invoice.DocumentMeta.Subject;
                document.Info.Keywords = invoice.DocumentMeta.Keywords;


                

                Document doc = invoice.CreateDocument();
               

                // Create a renderer and prepare (=layout) the document
                MigraDoc.Rendering.DocumentRenderer docRenderer = new DocumentRenderer(doc);
                docRenderer.PrepareDocument();
                
                int pageCount = docRenderer.FormattedDocument.PageCount;

                // For clarity we use point as unit of measure in this sample.
                // A4 is the standard letter size in Germany (21cm x 29.7cm).
                XRect A4Rect = new XRect(0, 0, A4Width, A4Height);

                for (int idx = 0; idx < pageCount; idx++)
                {
                    PdfPage page = document.AddPage();


                    // One page in Portrait...
                    //page.Size = new XSize(A4Width, A4Height);
                    page.Width = A4Width;
                    page.Height = A4Height;


                    



                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    // HACK²
                    gfx.MUH = PdfFontEncoding.Unicode;
                    //gfx.MFEH = PdfFontEmbedding.Default; iskomentarisano jer nije podrzano - depricated

                    //gfx.DrawString(pageSize.ToString(), font, XBrushes.DarkRed, new XRect(0, 0, page.Width, page.Height), XStringFormats.Center);

                    //XRect rect = GetRect(idx);

                    // Use BeginContainer / EndContainer for simplicity only. You can naturaly use you own transformations.
                    XGraphicsContainer container = gfx.BeginContainer(A4Rect, A4Rect, XGraphicsUnit.Point);

                    // Draw page border for better visual representation
                    //gfx.DrawRectangle(XPens.LightGray, A4Rect);

                    // Render the page. Note that page numbers start with 1.
                    docRenderer.RenderPage(gfx, idx + 1);

                    // Note: The outline and the hyperlinks (table of content) does not work in the produced PDF document.

                    // Pop the previous graphical state
                    gfx.EndContainer(container);
                }



                //// Use BeginContainer / EndContainer for simplicity only. You can naturaly use you own transformations.
                //XGraphicsContainer container = gfx.BeginContainer(A4Rect, A4Rect, XGraphicsUnit.Point);

                //// Draw page border for better visual representation
                ////gfx.DrawRectangle(XPens.LightGray, A4Rect);

                //// Render the page. Note that page numbers start with 1.
                //docRenderer.RenderPage(gfx, 1);

                //// Note: The outline and the hyperlinks (table of content) does not work in the produced PDF document.

                //// Pop the previous graphical state
                //gfx.EndContainer(container);


                //Debug.WriteLine("seconds=" + (DateTime.Now - now).TotalSeconds.ToString());

                // Save the document...
                document.Save(pdfFileFullPath);
                // ...and start a viewer
                //Process.Start(pdfFileFullPath);
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                //Console.ReadLine();
                throw new Exception(ex.Message);
            }
        }


    }
}
