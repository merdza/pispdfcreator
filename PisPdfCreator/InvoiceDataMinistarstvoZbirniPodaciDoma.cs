﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisPdfCreator
{
    public class InvoiceDataMinistarstvoZbirniPodaciDoma : InvoiceData
    {
        public string ImeDoma { get; set; }
        public string BrojRacuna { get; set; }
        public string BrojFakture { get; set; }
        public string DatumFakture { get; set; }

        public InvoiceDataMinistarstvoZbirniPodaciDoma(string imeDoma, string brojRacuna, string brojFakture, string datumFakture, string iznos):base(iznos)
        {
            ImeDoma = imeDoma;
            BrojRacuna = brojRacuna;
            BrojFakture = brojFakture;
            DatumFakture = datumFakture;
        }

    }
}
