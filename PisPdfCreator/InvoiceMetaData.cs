﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisPdfCreator
{
    public class InvoiceMetaData
    {
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoicePlaceOfIssuance { get; set; }
        public string PaymentTerms { get; set; }

        public InvoiceMetaData(string invoiceNumber, string invoiceDate, string invoicePlaceOfIssuance, string paymentTerms)
        {
            InvoiceNumber = invoiceNumber;
            InvoiceDate = invoiceDate;
            InvoicePlaceOfIssuance = invoicePlaceOfIssuance;
            PaymentTerms = paymentTerms;
        }
    }
}
